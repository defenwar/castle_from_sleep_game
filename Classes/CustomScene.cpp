#include "CustomScene.h"
#include "SceneFactory.h"

#define TAG_DEBUG_DRAW 4

using namespace castle;

Scene* CScene::createScene()
{
	return CScene::create();
}

bool CScene::init()
{
	bool bResult = Scene::init();
	m_pInfoButton = Button::create("info_button.png");

	bResult &= m_pInfoButton != nullptr;

	if (bResult)
	{
		this->addChild(m_pInfoButton, INT32_MAX);

		m_pInfoButton->setVisible(false);
		m_pInfoButton->setAnchorPoint(Vec2(1.f, 1.f));
		m_pInfoButton->setPosition(Vec2(this->getContentSize().width * 0.96f, this->getContentSize().height* 0.95f));
		m_pInfoButton->addTouchEventListener([this](Ref* sender, Widget::TouchEventType type)
		{
			if (type == ui::Widget::TouchEventType::ENDED && !m_bInfoBookIsOpened)
			{
				onShowInfoBook(true);
			}
		});

		m_VisibleSize = Director::getInstance()->getVisibleSize();
		m_VisibleOrigin = Director::getInstance()->getVisibleOrigin();

		auto scale = std::min(m_VisibleSize.height * 0.1f / m_pInfoButton->getContentSize().height, m_VisibleSize.width * 0.1f / m_pInfoButton->getContentSize().width);
		m_pInfoButton->setScale(scale);

		m_eventDispatcher = Director::getInstance()->getEventDispatcher();
		m_eventDispatcher->removeEventListenersForType(cocos2d::EventListener::Type::KEYBOARD);

		m_listenerKeyboard = EventListenerKeyboard::create();
		m_listenerKeyboard->onKeyReleased = CC_CALLBACK_2(CScene::onKeyReleased, this);
		m_eventDispatcher->addEventListenerWithFixedPriority(m_listenerKeyboard, 1);
	}

	return bResult;
}

void CScene::onEnter()
{
	Scene::onEnter();
}

void CScene::onShownGrayLayer(Color3B color, float fTime)
{
	auto  grayLayer = Sprite::create("pixel.png");

	if (!isBackroundCallBack())
	{
		setNextScene(sz_previewScene);
	}

	if (grayLayer != nullptr)
	{
		this->addChild(grayLayer, INT32_MAX);


		grayLayer->setColor(color);
		grayLayer->setAnchorPoint(Vec2(0.f, 0.f));
		grayLayer->setContentSize(m_VisibleSize);
		grayLayer->runAction(FadeOut::create(0.f));
		grayLayer->setCascadeOpacityEnabled(true);

		grayLayer->runAction(Sequence::create(
			FadeIn::create(fTime)
			, DelayTime::create(0.2f)
			, CallFunc::create(m_onBackGroundEnd)
			, nullptr));
	}
	else
	{
		m_onBackGroundEnd();
	}
}

void CScene::createInfoBook(const std::string szName)
{
	if (m_pInfoBook == nullptr)
	{
		m_pInfoBook = Sprite::create(szName);
		this->addChild(m_pInfoBook, INT32_MAX);
	}

	if (m_pInfoButtonClose == nullptr)
		m_pInfoButtonClose = Button::create("close_button.png");

	if (m_pInfoButtonClose != nullptr)
	{
		m_pInfoBook->addChild(m_pInfoButtonClose);

		m_pInfoButtonClose->setAnchorPoint(Vec2(1.f, 1.f));
		m_pInfoButtonClose->setPosition(Vec2(m_pInfoBook->getContentSize().width * 0.95f, m_pInfoBook->getContentSize().height* 0.95f));
		m_pInfoButtonClose->setTouchEnabled(false);
		m_pInfoButtonClose->addTouchEventListener([this](Ref* sender, Widget::TouchEventType type)
		{
			if (type == ui::Widget::TouchEventType::ENDED)
			{
				onShowInfoBook(false);
			}
		});
	}
	auto sizePage = Size((m_pInfoBook->getContentSize().width) / 2.38f, m_pInfoBook->getContentSize().height / 1.12f);

	if (m_pLeftPage != nullptr)
	{
		m_pInfoBook->addChild(m_pLeftPage);

		auto scale = std::min(sizePage.width / m_pLeftPage->getContentSize().width, sizePage.height / m_pLeftPage->getContentSize().height);
		m_pLeftPage->setScale(scale);
		m_pLeftPage->setPosition(Vec2(m_pInfoBook->getContentSize().width / 3.6f, m_pInfoBook->getContentSize().height / 2.f));
	}

	if (m_pRightPage != nullptr)
	{
		m_pInfoBook->addChild(m_pRightPage);

		auto scale = std::min(sizePage.width / m_pRightPage->getContentSize().width, sizePage.height / m_pRightPage->getContentSize().height);
		m_pRightPage->setScale(scale);
		m_pRightPage->setPosition(Vec2(m_pInfoBook->getContentSize().width / 1.37f, m_pInfoBook->getContentSize().height / 2.f));
	}

	m_pInfoBook->setPosition(Vec2(m_VisibleSize.width / 2 + m_VisibleOrigin.x, m_VisibleSize.height / 2 + m_VisibleOrigin.y));
	auto scale = std::min(m_VisibleSize.height / m_pInfoBook->getContentSize().height, m_VisibleSize.width / m_pInfoBook->getContentSize().width);
	m_pInfoBook->setScale(scale);
	setCascadeOpacityRecursive(m_pInfoBook, true);
	m_pInfoBook->runAction(FadeOut::create(0.f));
}

void castle::CScene::onShowInfoBook(bool bShow)
{
	if (m_pInfoBook != nullptr);
	{
		if (bShow && !m_bInfoBookIsOpened)
		{
			m_bInfoBookIsOpened = true;
			m_pInfoButtonClose->setTouchEnabled(m_bInfoBookIsOpened);
			m_pInfoButton->setTouchEnabled(!m_bInfoBookIsOpened);

			m_pInfoButton->runAction(FadeOut::create(0.25f));
			m_pInfoBook->runAction(Sequence::create(
				FadeIn::create(0.25f)
				, CallFunc::create(CC_CALLBACK_0(castle::CScene::onOpenedBook, this))
				, nullptr));
		}
		if (!bShow && m_bInfoBookIsOpened)
		{
			m_bInfoBookIsOpened = false;
			m_pInfoButtonClose->setTouchEnabled(m_bInfoBookIsOpened);
			m_pInfoButton->setTouchEnabled(!m_bInfoBookIsOpened);

			m_pInfoButton->runAction(FadeIn::create(0.25f));
			m_pInfoBook->runAction(Sequence::create(
				FadeOut::create(0.25f)
				, CallFunc::create(CC_CALLBACK_0(castle::CScene::onClosedBook, this))
				, nullptr));
		}
	}
}

void castle::CScene::setNextScene(std::string const & name)
{
	m_onBackGroundEnd = [name]()
	{
		auto factory = new CSceneFactory();
		auto nextScene = factory->createScene(name);

		Director::getInstance()->replaceScene(nextScene);
	};
}

bool castle::CScene::setBackground(std::string const & path)
{
	m_pBackground = Sprite::create(path);
	bool bResult = m_pBackground != nullptr;

	if (bResult)
	{
		this->addChild(m_pBackground, -4);

		m_pBackground->setPosition(Vec2(m_VisibleSize.width / 2 + m_VisibleOrigin.x, m_VisibleSize.height / 2 + m_VisibleOrigin.y));
		auto scale = std::max(m_VisibleSize.height / m_pBackground->getContentSize().height, m_VisibleSize.width / m_pBackground->getContentSize().width);
		m_pBackground->setScale(scale);
	}

	return bResult;
}

void castle::CScene::setCopySizeScalePosition(Node * node, const Node * nodeCopied)
{
	node->setContentSize(nodeCopied->getContentSize());
	node->setScale(nodeCopied->getScale());
	node->setPosition(nodeCopied->getPosition());
}

Vec2 castle::CScene::getPositionAboutParrent(const Node * node, Vec2 posPercent)
{
	auto currentNodeSize = node->getParent()->getContentSize();
	return Vec2(currentNodeSize.width * posPercent.x, currentNodeSize.height * posPercent.y);
}

void CScene::drawAllNode(const cocos2d::Vector<Node*>& nodes)
{
	for (const auto& children : nodes)
	{
		if (children->getTag() != TAG_DEBUG_DRAW)
		{
			auto pDrawNode = DrawNode::create();
			if (pDrawNode != nullptr)
			{
				Color4F color;
				color = Color4F(cocos2d::random(0.f, 1.f) * 0.5f + 0.5f,
					cocos2d::random(0.f, 1.f) * 0.5f + 0.5f,
					cocos2d::random(0.f, 1.f) * 0.5f + 0.5f,
					1);
				pDrawNode->drawRect(Point::ZERO, children->getContentSize(), color);
				pDrawNode->setAnchorPoint(Point::ZERO);

				children->addChild(pDrawNode, INT32_MAX, TAG_DEBUG_DRAW);
			}

			if (!children->getChildren().empty())
			{
				drawAllNode(children->getChildren());
			}
		}
	}
}

void CScene::removeDrawAllNode(const cocos2d::Vector<Node*>& nodes)
{
	for (const auto& children : nodes)
	{
		if (children->getTag() != TAG_DEBUG_DRAW)
		{
			children->removeChildByTag(TAG_DEBUG_DRAW);

			if (!children->getChildren().empty())
			{
				removeDrawAllNode(children->getChildren());
			}
		}
	}
}

void CScene::setCascadeOpacityRecursive(Node* node, const bool& bEnable)
{
	node->setCascadeOpacityEnabled(true);

	for (const auto& children : node->getChildren())
	{
		setCascadeOpacityRecursive(children, bEnable);
	}
}


void CScene::onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event)
{
	switch (keyCode)
	{
	case EventKeyboard::KeyCode::KEY_F9:
	{
		if (!m_bShownDrawNodes)
			drawAllNode(this->getChildren());
		else
			removeDrawAllNode(this->getChildren());

		m_bShownDrawNodes = !m_bShownDrawNodes;
	}
	break;
	case EventKeyboard::KeyCode::KEY_ESCAPE:
	case EventKeyboard::KeyCode::KEY_L:
	{
		onShownGrayLayer(Color3B::BLACK, 0.f);
	}
	break;
	case EventKeyboard::KeyCode::KEY_R:
	{
		if (!m_szSceneName.empty())
		{
			setNextScene(m_szSceneName);
			onShownGrayLayer(Color3B::BLACK, 0.f);
		}
	}
	break;
	default:
	{;; }
	}
}
