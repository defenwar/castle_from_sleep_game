#pragma once
#include "AppConstants.h"
#include "cocos2d.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
using namespace ui;
namespace castle
{
	class CSceneFactory
	{
	public:
		CSceneFactory() = default;
		virtual ~CSceneFactory() = default;

		Scene* createScene(std::string const& name);
	};
}
