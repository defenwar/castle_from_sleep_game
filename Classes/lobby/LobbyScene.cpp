#include "lobby/LobbyScene.h"

using namespace castle;

CScene* LobbyScene::createScene()
{
	return LobbyScene::create();
}

bool LobbyScene::init()
{
	bool bResult = CScene::init();

	m_szSceneName = sz_lobbyScene;

	m_pScroller = ScrollView::create();
	m_pTopInterface = Sprite::create("lobby/top_interface.png");
	m_pBottomInterface = Sprite::create("lobby/bottom_interface.png");
	auto m_pTextWidgetTop = Label::createWithTTF("- Have a nice game -", "fonts/marker-felt.ttf", 80);
	auto m_pTextWidgetBottom = Label::createWithTTF("- Scroll through the list and choose a room -", "fonts/marker-felt.ttf", 80);

	bResult &= setBackground("lobby/lobby_background.png");
	bResult &= m_pScroller != nullptr;
	bResult &= m_pTopInterface != nullptr;
	bResult &= m_pBottomInterface != nullptr;
	bResult &= m_pTextWidgetTop != nullptr;
	bResult &= m_pTextWidgetBottom != nullptr;

	if (bResult)
	{
		this->addChild(m_pTopInterface, 0);
		this->addChild(m_pBottomInterface, 1);
		this->addChild(m_pScroller, 2);

		auto scaleTop = m_VisibleSize.width / m_pTopInterface->getContentSize().width;
		m_pTopInterface->setScale(scaleTop);
		auto scaleBottom = m_VisibleSize.width / m_pBottomInterface->getContentSize().width;
		m_pBottomInterface->setScale(scaleBottom);

		m_pTopInterface->setAnchorPoint(Vec2(0.5f, 1.0f));
		m_pBottomInterface->setAnchorPoint(Vec2(0.5f, 0.0f));

		m_pTopInterface->setPosition(Vec2(m_VisibleSize.width / 2 + m_VisibleOrigin.x, m_VisibleSize.height));
		m_pBottomInterface->setPosition(Vec2(m_VisibleSize.width / 2 + m_VisibleOrigin.x, 0.f));

		m_pTopInterface->addChild(m_pTextWidgetTop);
		m_pBottomInterface->addChild(m_pTextWidgetBottom);

		m_pTextWidgetTop->setTextColor(Color4B(255, 215, 0, 255));
		m_pTextWidgetBottom->setTextColor(Color4B(255, 215, 0, 255));
		m_pTextWidgetTop->enableOutline(Color4B(184, 134, 11, 255), 5);
		m_pTextWidgetBottom->enableOutline(Color4B(184, 134, 11, 255), 5);

		m_pTextWidgetTop->setPosition(getPositionAboutParrent(m_pTextWidgetTop, Vec2(0.5, 0.55f)));
		m_pTextWidgetBottom->setPosition(getPositionAboutParrent(m_pTextWidgetBottom, Vec2(0.5, 0.45f)));

		m_pScroller->setDirection(ScrollView::Direction::HORIZONTAL);
		m_pScroller->setPosition(Vec2(m_VisibleSize.width / 2 + m_VisibleOrigin.x, m_VisibleSize.height / 2 + m_VisibleOrigin.y));
		m_pScroller->setAnchorPoint(Vec2(0.5f, 0.5f));
		auto heightSizeScroller = m_VisibleSize.height - 
			(m_pTopInterface->getContentSize().height * m_pTopInterface->getScale() * 1.1f) -
			(m_pBottomInterface->getContentSize().height * m_pBottomInterface->getScale() * 1.1f);

		m_pScroller->setContentSize(Size(m_VisibleSize.width, heightSizeScroller));
		m_pScroller->setBounceEnabled(true);
	}

	if (bResult)
	{
		bResult &= addLobbyElement("lobby/preview/doors_preview.png",sz_doorsScene);
		bResult &= addLobbyElement("lobby/preview/mramor_preview.png", sz_mramorScene);
		bResult &= addLobbyElement("lobby/preview/two_pictures_preview.png", sz_twoPicturesScene);
		bResult &= addLobbyElement("lobby/preview/librari_preview.png", sz_libraryScene);
		bResult &= addLobbyElement("lobby/preview/pat_preview.png", sz_patScene);

		float sizeWidgth = 50.f;
		for (const auto& element: m_pScroller->getChildren())
		{
			sizeWidgth += element->getContentSize().width * element->getScale() + 50.f;
		}
		m_pScroller->setInnerContainerSize(Size(sizeWidgth, m_pScroller->getContentSize().height));
	}

	return bResult;
}

void LobbyScene::onEnter()
{
	CScene::onEnter();
}

bool castle::LobbyScene::addLobbyElement(const std::string & szImage, const std::string & szScene)
{
	bool bResult = true;
	auto newElement = Button::create(szImage);
	bResult &= newElement != nullptr;

	if (bResult)
	{
		m_pScroller->addChild(newElement);
		newElement->setZoomScale(0.f);
		auto scale = m_pScroller->getContentSize().height / newElement->getContentSize().height;
		newElement->setScale(scale);
		newElement->setAnchorPoint(Vec2(0.0f, 0.5f));
		newElement->setPosition(Vec2( 50.f+ m_nElementsCount *(newElement->getContentSize().width * scale + 50.f), m_pScroller->getContentSize().height / 2));
		newElement->addTouchEventListener([this, szScene](Ref* sender, Widget::TouchEventType type)
		{
			if (!m_bProccesed)
			{
				switch (type)
				{
				case ui::Widget::TouchEventType::ENDED:
				{
					m_bProccesed = true;
					setNextScene(szScene);
					onShownGrayLayer(Color3B::BLACK, 0.35f);
				}
					break;
				default:
					break;
				}
			}
		});
		m_nElementsCount++;
	}

	return bResult;
}
