#include "pch.h"

namespace castle
{
	class LobbyScene : public CScene
	{
	public:
		LobbyScene() = default;
		virtual ~LobbyScene() = default;

	public:
		static CScene* createScene();

		virtual bool init();

		virtual void onEnter();

		CREATE_FUNC(LobbyScene);

	private:
		bool addLobbyElement(const std::string & szImage, const std::string & szScene);

	protected:
		bool m_bProccesed = false;

		Label* m_pTextWidgetTop = nullptr;
		Label* m_pTextWidgetBottom = nullptr;

		uint32_t m_nElementsCount;
		ScrollView* m_pScroller = nullptr;
		Sprite* m_pTopInterface = nullptr;
		Sprite* m_pBottomInterface = nullptr;
	};
}
