#include "pch.h"

namespace castle
{
	class PreviewScene : public CScene
	{
	public:
		PreviewScene() = default;
		virtual ~PreviewScene() = default;

	public:
		static CScene* createScene();

		virtual bool init();

		virtual void onEnter();

		CREATE_FUNC(PreviewScene);

	protected:
		Sprite* m_pOtherLogo;
		Label* m_pLabel = nullptr;
		Sprite* m_pLogo = nullptr;
		//LoadingBar* m_pLoadingBar;
	};
}