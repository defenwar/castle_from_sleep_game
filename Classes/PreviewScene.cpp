#include "pch.h"
#include "PreviewScene.h"

CScene* PreviewScene::createScene()
{
    return PreviewScene::create();
}

bool PreviewScene::init()
{
	bool bResult = CScene::init();

	m_szSceneName = sz_previewScene;

	m_pLabel = Label::createWithTTF("Castle from sleep", "fonts/marker-felt.ttf", 85);
	m_pLogo = Sprite::create("preview.png");
	m_pOtherLogo = Sprite::create("preview_loader.png");
	
	bResult &= m_pLabel != nullptr;
	bResult &= m_pLogo != nullptr;
	bResult &= m_pOtherLogo != nullptr;

	if (bResult)
	{
		this->addChild(m_pLabel, 1);
		this->addChild(m_pLogo, 0);
		m_pLogo->addChild(m_pOtherLogo, 0);

		m_pLabel->setColor(Color3B::GRAY);
		m_pLabel->enableGlow(Color4B::WHITE);
		m_pLabel->setAnchorPoint(Vec2(0.5f, 0.f));
		m_pLabel->setPosition(Vec2(m_VisibleOrigin.x + m_VisibleSize.width / 2, 0.f));
		m_pLabel->setScale(std::min(((m_VisibleSize.width / m_pLabel->getContentSize().width) * 0.9f), 1.f));

		m_pLogo->setAnchorPoint(Vec2(0.5f, 1.f));
		m_pLogo->setPosition(Vec2(m_VisibleSize.width / 2 + m_VisibleOrigin.x, m_VisibleSize.height));
		auto scale = std::min((m_VisibleSize.height - m_pLabel->getContentSize().height ) / m_pLogo->getContentSize().height
			, m_VisibleSize.width / m_pLogo->getContentSize().width);
		m_pLogo->setScale(scale);

		auto pos = Vec2(0.5647f, 0.6625f);

		pos.x *= m_pOtherLogo->getParent()->getContentSize().width;
		pos.y *= m_pOtherLogo->getParent()->getContentSize().height;
		m_pOtherLogo->setPosition(pos);
		m_pOtherLogo->runAction(FadeOut::create(0.f));

		if (!isBackroundCallBack())
		{
			setNextScene(sz_lobbyScene);
		}
	}

	return bResult;
}

void PreviewScene::onEnter()
{
	CScene::onEnter();

	float fDelay = 0.5f;
	m_pOtherLogo->runAction(Sequence::create(
		FadeIn::create(1.f)
		, DelayTime::create(fDelay)
		, CallFunc::create(CC_CALLBACK_0(CScene::onShownGrayLayer, this, Color3B::BLACK, 0.35f))
		, nullptr));
}
