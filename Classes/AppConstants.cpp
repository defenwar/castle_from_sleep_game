#include "pch.h"
#include "AppConstants.h"

namespace castle
{
	const std::string sz_previewScene = "previewScene";
	const std::string sz_lobbyScene = "lobbyScene";
	const std::string sz_doorsScene = "doorsScene";
	const std::string sz_mramorScene = "mramorScene";
	const std::string sz_twoPicturesScene = "twoPicturesScene";
	const std::string sz_libraryScene = "libraryScene";
	const std::string sz_patScene = "patScene";
}