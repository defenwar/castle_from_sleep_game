#pragma once
#include <string>

namespace castle
{
	extern const std::string sz_lobbyScene;
	extern const std::string sz_previewScene;
	extern const std::string sz_doorsScene;
	extern const std::string sz_mramorScene;
	extern const std::string sz_twoPicturesScene;
	extern const std::string sz_libraryScene;
	extern const std::string sz_patScene;
}