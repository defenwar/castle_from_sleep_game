#include "SceneFactory.h"
#include "SceneHeaders.h"

using namespace castle;

Scene* CSceneFactory::createScene(std::string const& name)
{		
	if (name == sz_lobbyScene)
		return LobbyScene::createScene();
	if (name == sz_previewScene)
		return PreviewScene::createScene();
	if (name == sz_doorsScene)
		return DoorsRoomScene::createScene();
	if (name == sz_mramorScene)
		return MramorRoomScene::createScene();
	if (name == sz_twoPicturesScene)
		return TwoPicturesScene::createScene();
	if (name == sz_libraryScene)
		return LibraryScene::createScene();
	if (name == sz_patScene)
		return PatScene::createScene();


	return PreviewScene::createScene();
}
