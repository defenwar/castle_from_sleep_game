#include "cocos2d.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
using namespace ui;

namespace castle
{
	class CScene : public cocos2d::Scene
	{
	public:
		CScene() = default;
		virtual ~CScene() = default;

	public:
		static cocos2d::Scene* createScene();

		virtual bool init();
		virtual void onEnter();

		virtual void onClosedBook() { ;; };
		virtual void onOpenedBook() { ;; };
		void onShownGrayLayer(Color3B color, float fTime);
		void createInfoBook(const std::string szName);
		void onShowInfoBook(bool bShow);
		void setNextScene(std::string const& name);
		bool setBackground(std::string const& path);
		void setCopySizeScalePosition(Node* node, const Node* nodeCopied);
		bool isBackroundCallBack() { return m_onBackGroundEnd != nullptr;};
		Vec2 getPositionAboutParrent(const Node* node, Vec2 posPercent);

		CREATE_FUNC(CScene);

	private:
		void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);
		void drawAllNode(const cocos2d::Vector<Node*>& nodes);
		void removeDrawAllNode(const cocos2d::Vector<Node*>& nodes);

		void setCascadeOpacityRecursive(Node * node, const bool & bEnable);

	public:
		std::string m_szSceneName;
		Sprite* m_pBackground = nullptr;

		bool m_bInfoBookIsOpened = false;
		Button* m_pInfoButton = nullptr;
		Button* m_pInfoButtonClose = nullptr;
		Sprite* m_pInfoBook = nullptr;
		Node* m_pLeftPage = nullptr;
		Node* m_pRightPage = nullptr;

	protected:
		cocos2d::Size m_VisibleSize;
		Vec2 m_VisibleOrigin;

	private:
		cocos2d::EventDispatcher* m_eventDispatcher = nullptr;
		cocos2d::EventListenerKeyboard* m_listenerKeyboard = nullptr;

		bool m_bShownDrawNodes = false;

		std::function<void()> m_onBackGroundEnd = nullptr;
	};
}
