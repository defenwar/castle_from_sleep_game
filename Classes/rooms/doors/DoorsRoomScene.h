#include "pch.h"

namespace castle
{
	class DoorsRoomScene : public CScene
	{
	public:
		DoorsRoomScene() = default;
		virtual ~DoorsRoomScene() = default;

	public:
		static CScene* createScene();

		virtual bool init();

		virtual void onEnter();

		bool initBook();

		CREATE_FUNC(DoorsRoomScene);

	protected:
		bool m_bNotPressed = true;
		Button* m_pDoorLeft = nullptr;
		Button* m_pDoorCenter = nullptr;
		Button* m_pDoorRight = nullptr;

		Label* m_pTextBook1 = nullptr;
		Label* m_pTextBook2 = nullptr;
		Layout* m_pLayoutButtons = nullptr;
		Button* m_pGreenButton = nullptr;
		Button* m_pRedButton = nullptr;
	};
}