#include "rooms/doors/DoorsRoomScene.h"

using namespace castle;

CScene* DoorsRoomScene::createScene()
{
	return DoorsRoomScene::create();
}

bool DoorsRoomScene::init()
{
	bool bResult = CScene::init();
	
	m_szSceneName = sz_doorsScene;

	m_pDoorLeft = Button::create("rooms/doors/door_left_close.png", "rooms/doors/door_left_open.png");
	m_pDoorCenter = Button::create("rooms/doors/door_center_close.png", "rooms/doors/door_center_open.png");
	m_pDoorRight = Button::create("rooms/doors/door_right_close.png", "rooms/doors/door_right_open.png");

	bResult &= setBackground("rooms/doors/doors_bg.png");
	bResult &= m_pDoorLeft != nullptr;
	bResult &= m_pDoorCenter != nullptr;
	bResult &= m_pDoorRight != nullptr;

	bResult &= initBook();

	if (bResult)
	{
		m_pBackground->addChild(m_pDoorLeft, 1);
		m_pBackground->addChild(m_pDoorCenter, 1);
		m_pBackground->addChild(m_pDoorRight, 1);
		
		m_pDoorLeft->setPosition(getPositionAboutParrent(m_pDoorLeft, Vec2(0.24, 0.43)));
		m_pDoorCenter->setPosition(getPositionAboutParrent(m_pDoorCenter, Vec2(0.46, 0.483)));
		m_pDoorRight->setPosition(getPositionAboutParrent(m_pDoorRight, Vec2(0.66, 0.415)));

		auto callBack = [this](Ref* sender, Widget::TouchEventType type)
		{
			if (type == ui::Widget::TouchEventType::ENDED && !m_bInfoBookIsOpened)
			{
				m_pDoorLeft->setTouchEnabled(false);
				m_pDoorCenter->setTouchEnabled(false);
				m_pDoorRight->setTouchEnabled(false);

				m_pTextBook1->setVisible(false);
				m_pTextBook2->setVisible(true);
				m_pLayoutButtons->setVisible(true);

				m_pInfoButton->setVisible(false);
				m_pInfoButtonClose->setVisible(false);
				onShowInfoBook(true);
			}
		};

		m_pDoorLeft->addTouchEventListener(callBack);
		m_pDoorCenter->addTouchEventListener(callBack);
		m_pDoorRight->addTouchEventListener(callBack);
	}

	return bResult;
}

void DoorsRoomScene::onEnter()
{
	CScene::onEnter();
	if (m_pInfoButton != nullptr)
		m_pInfoButton->setVisible(true);
}

bool DoorsRoomScene::initBook()
{
	m_pTextBook1 = Label::createWithTTF("This is some kind of room with doors, nothing can be seen behind the door.It is worth trying to open one of these doors", "fonts/marker-felt.ttf", 85);
	m_pTextBook2 = Label::createWithTTF("A voice asked, \"Am I sure I want to go through this door ? \"", "fonts/marker-felt.ttf", 85);
	auto textGreenButton = Label::createWithTTF("No", "fonts/marker-felt.ttf", 80);
	auto textRedButton = Label::createWithTTF("Yes", "fonts/marker-felt.ttf", 80);
	auto layoutTexts = Layout::create();
	m_pLayoutButtons = Layout::create();
	m_pGreenButton = Button::create("button_idle.png", "button_push.png");
	m_pRedButton = Button::create("button_idle.png", "button_push.png");

	bool bResult = m_pTextBook1 != nullptr;
	bResult = m_pTextBook2 != nullptr;
	bResult = layoutTexts != nullptr;
	bResult = m_pLayoutButtons != nullptr;
	bResult = m_pGreenButton != nullptr;
	bResult = m_pRedButton != nullptr;
	bResult = textGreenButton != nullptr;
	bResult = textRedButton != nullptr;

	if (bResult)
	{
		m_pTextBook1->setDimensions(700, 900);
		m_pTextBook2->setDimensions(700, 900);
		m_pTextBook1->setColor(Color3B::BLACK);
		m_pTextBook2->setColor(Color3B::BLACK);
		layoutTexts->addChild(m_pTextBook1);
		layoutTexts->addChild(m_pTextBook2);

		textGreenButton->setColor(Color3B::BLACK);
		textRedButton->setColor(Color3B::BLACK);

		m_pGreenButton->setTitleLabel(textGreenButton);
		m_pRedButton->setTitleLabel(textRedButton);

		m_pLeftPage = layoutTexts;

		layoutTexts->setContentSize(Size(700, 900));
		layoutTexts->setAnchorPoint(Vec2(0.5, 0.5));

		m_pTextBook1->setAnchorPoint(Vec2(0.5, 0.5));
		m_pTextBook2->setAnchorPoint(Vec2(0.5, 0.5));

		m_pTextBook1->setPosition(getPositionAboutParrent(m_pTextBook1, Vec2(0.5, 0.5)));
		m_pTextBook2->setPosition(getPositionAboutParrent(m_pTextBook2, Vec2(0.5, 0.5)));

		m_pTextBook2->setVisible(false);

		m_pLayoutButtons->setContentSize(Size(700, 900));
		m_pLayoutButtons->setAnchorPoint(Vec2(0.5, 0.5));

		m_pLayoutButtons->addChild(m_pGreenButton, INT32_MAX);
		m_pLayoutButtons->addChild(m_pRedButton, INT32_MAX);

		m_pGreenButton->setColor(Color3B::GREEN);
		m_pRedButton->setColor(Color3B::RED);

		m_pGreenButton->setAnchorPoint(Vec2(0.5, 0.5));
		m_pRedButton->setAnchorPoint(Vec2(0.5, 0.5));

		m_pGreenButton->setScale(1.25f);
		m_pRedButton->setScale(1.25f);

		m_pGreenButton->setPosition(getPositionAboutParrent(m_pGreenButton, Vec2(0.5, 0.7)));
		m_pRedButton->setPosition(getPositionAboutParrent(m_pRedButton, Vec2(0.5, 0.3)));

		auto callBackButtons = [this](Ref* sender, Widget::TouchEventType type)
		{
			if (type == ui::Widget::TouchEventType::ENDED && m_bNotPressed)
			{
				if (m_pGreenButton->_ID == sender->_ID)
				{
					m_bNotPressed = false;
					setNextScene(m_szSceneName);
					onShownGrayLayer(Color3B::WHITE, 0.45f);
				}

				if (m_pRedButton->_ID == sender->_ID)
				{
					m_bNotPressed = false;
					onShownGrayLayer(Color3B::BLACK, 0.5f);
				}
			}
		};

		m_pGreenButton->addTouchEventListener(callBackButtons);
		m_pRedButton->addTouchEventListener(callBackButtons);

		m_pLayoutButtons->setVisible(false);

		m_pRightPage = m_pLayoutButtons;
	}

	createInfoBook("book_1.png");

	return bResult;
}