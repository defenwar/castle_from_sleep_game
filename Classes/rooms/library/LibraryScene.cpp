#include "rooms/library/LibraryScene.h"

using namespace castle;

static std::mt19937& getRandomEngine()
{
	static std::random_device seed_gen;
	static std::mt19937 engine(seed_gen());
	return engine;
}

CScene* LibraryScene::createScene()
{
	return LibraryScene::create();
}

bool LibraryScene::init()
{
	bool bResult = CScene::init();

	std::string szInfo;
	m_szSceneName = sz_libraryScene;

	m_pBookcase = Sprite::create("rooms/library/library_empty_bookcase.png");

	m_pInfoTextWidget = Label::createWithTTF("When I went into the room I saw a sequence of numbers. If I pull out the wrong books 5 times, then again I get into this room, but with different numbers.", "fonts/marker-felt.ttf", 85);
	auto textWidget2 = Label::createWithTTF("Congratulations!", "fonts/marker-felt.ttf", 85);
	auto textOK = Label::createWithTTF("OK!", "fonts/marker-felt.ttf", 65);
	m_pButtonExit = Button::create("button_idle.png", "button_push.png");
	auto layoutTextAndButton = Layout::create();
	auto layoutTwoTexts = Layout::create();

	bResult &= setBackground("rooms/library/library_background_2.png");
	bResult &= m_pBookcase != nullptr;
	bResult &= m_pInfoTextWidget != nullptr;
	bResult &= textWidget2 != nullptr;
	bResult &= m_pButtonExit != nullptr;
	bResult &= textOK != nullptr;
	bResult &= layoutTextAndButton != nullptr;

	if (bResult)
	{
		bResult &= addBook("rooms/library/books/book_15.png", Vec2(0.21f, 0.635f));
		bResult &= addBook("rooms/library/books/book_16.png", Vec2(0.3f, 0.635f));
		bResult &= addBook("rooms/library/books/book_17.png", Vec2(0.43f, 0.635f));
		bResult &= addBook("rooms/library/books/book_18.png", Vec2(0.55f, 0.635f));
		bResult &= addBook("rooms/library/books/book_19.png", Vec2(0.65f, 0.635f));
		bResult &= addBook("rooms/library/books/book_20.png", Vec2(0.762f, 0.635f));

		bResult &= addBook("rooms/library/books/book_8.png", Vec2(0.21f, 0.342f));
		bResult &= addBook("rooms/library/books/book_9.png", Vec2(0.3f, 0.342f));
		bResult &= addBook("rooms/library/books/book_10.png", Vec2(0.43f, 0.342f));
		bResult &= addBook("rooms/library/books/book_11.png", Vec2(0.5f, 0.342f));
		bResult &= addBook("rooms/library/books/book_12.png", Vec2(0.642f, 0.342f));
		bResult &= addBook("rooms/library/books/book_13.png", Vec2(0.7f, 0.342f));
		bResult &= addBook("rooms/library/books/book_14.png", Vec2(0.76f, 0.342f));

		bResult &= addBook("rooms/library/books/book_1.png", Vec2(0.21f, 0.06f));
		bResult &= addBook("rooms/library/books/book_2.png", Vec2(0.3f, 0.06f));
		bResult &= addBook("rooms/library/books/book_3.png", Vec2(0.433f, 0.06f));
		bResult &= addBook("rooms/library/books/book_4.png", Vec2(0.5f, 0.06f));
		bResult &= addBook("rooms/library/books/book_5.png", Vec2(0.6f, 0.06f));
		bResult &= addBook("rooms/library/books/book_6.png", Vec2(0.7f, 0.06f));
		bResult &= addBook("rooms/library/books/book_7.png", Vec2(0.75f, 0.06f));

		std::shuffle(m_vBookNumbers.begin(), m_vBookNumbers.end(), getRandomEngine());

		m_nNeedOpen = std::min(5u, m_vBookNumbers.size());

		for (uint32_t indx = 0; indx < m_nNeedOpen; indx++)
		{
			m_vCurrentNumbers.push_back(m_vBookNumbers[indx]);

			if (indx != 0u)
			{
				szInfo += ", ";
			}

			szInfo += std::to_string(m_vCurrentNumbers[indx]);
		}
	}

	m_pFirstTextWidget = Label::createWithTTF(szInfo, "fonts/marker-felt.ttf", 85);
	bResult &= m_pFirstTextWidget != nullptr;

	if (bResult)
	{
		this->addChild(m_pBookcase, 0);
		m_pBookcase->setPosition(Vec2(m_VisibleSize.width / 2 + m_VisibleOrigin.x, m_VisibleSize.height / 2 + m_VisibleOrigin.y));
		m_pBookcase->setScale(m_VisibleSize.height / m_pBookcase->getContentSize().height);

		m_pInfoTextWidget->setDimensions(700, 900);

		layoutTwoTexts->setContentSize(Size(700, 900));
		layoutTextAndButton->setContentSize(Size(700, 900));
		layoutTwoTexts->setAnchorPoint(Vec2(0.5, 0.5));
		layoutTextAndButton->setAnchorPoint(Vec2(0.5, 0.5));

		layoutTextAndButton->addChild(textWidget2);
		layoutTextAndButton->addChild(m_pButtonExit);
		layoutTwoTexts->addChild(m_pFirstTextWidget);
		layoutTwoTexts->addChild(m_pInfoTextWidget);

		m_pInfoTextWidget->setColor(Color3B::BLACK);
		m_pFirstTextWidget->setColor(Color3B::BLACK);
		textWidget2->setColor(Color3B::BLACK);
		textOK->setColor(Color3B(200, 200, 200));

		auto sizePage = layoutTwoTexts->getContentSize();
		m_pInfoTextWidget->setScale(std::min(sizePage.width / m_pInfoTextWidget->getContentSize().width, sizePage.height / m_pInfoTextWidget->getContentSize().height));
		m_pFirstTextWidget->setScale(std::min(sizePage.width / m_pFirstTextWidget->getContentSize().width, sizePage.height / m_pFirstTextWidget->getContentSize().height));
		m_pInfoTextWidget->setPosition(Vec2(sizePage.width /2.f, sizePage.height / 2.f));
		m_pFirstTextWidget->setPosition(Vec2(sizePage.width /2.f, sizePage.height / 2.f));

		textWidget2->setPosition(getPositionAboutParrent(textWidget2, Vec2(0.5, 0.75f)));

		m_pButtonExit->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_pButtonExit->setColor(Color3B(105, 160, 255));
		m_pButtonExit->setPosition(getPositionAboutParrent(m_pButtonExit, Vec2(0.5, 0.25f)));

		m_pButtonExit->setTitleLabel(textOK);
		m_pButtonExit->setScale(1.5f);

		m_pButtonExit->addTouchEventListener([this](Ref* sender, Widget::TouchEventType type)
		{
			if (type == ui::Widget::TouchEventType::ENDED)
			{
				m_pButtonExit->setTouchEnabled(false);
				onShownGrayLayer(Color3B::BLACK, 0.5f);
			}
		});

		layoutTextAndButton->setVisible(false);
		m_pInfoTextWidget->setVisible(false);

		m_pLeftPage = layoutTwoTexts;
		m_pRightPage = layoutTextAndButton;

		createInfoBook("book_1.png");
	}

	return bResult;
}

void castle::LibraryScene::onClosedBook()
{
	m_pFirstTextWidget->setVisible(false);
	m_pInfoTextWidget->setVisible(true);
}

void LibraryScene::onEnter()
{
	CScene::onEnter();
	if (m_pInfoButton != nullptr)
		m_pInfoButton->setVisible(true);

	onShowInfoBook(true);
}

bool LibraryScene::addBook(const std::string& szPath, Vec2 vPosition)
{
	uint32_t nBookNumber = cocos2d::random<uint32_t>(1u, 99u);

	m_vBookNumbers.push_back(nBookNumber);

	auto pBook = Button::create(szPath);
	auto pBookText = Label::create( std::to_string(nBookNumber), "fonts/marker-felt.ttf", 55);

	bool bResult = pBook != nullptr;
	bResult = pBookText != nullptr;

	if (bResult)
	{
		pBookText->setColor(Color3B::WHITE);
		//pBookText->setDimensions(pBook->getContentSize().width, pBook->getContentSize().height);

		m_pBookcase->addChild(pBook);
		pBook->setAnchorPoint(Vec2(0.5f, 0.f));
		pBook->setPosition(getPositionAboutParrent(pBook, vPosition));
		pBook->setTitleLabel(pBookText);
		pBook->addTouchEventListener(CC_CALLBACK_2(castle::LibraryScene::clickBook, this));
		m_vBooks.emplace(pBook, nBookNumber);
	}

	return bResult;
}

void castle::LibraryScene::clickBook(Ref * sender, Widget::TouchEventType type)
{
	auto onEnd = [this]()
	{
		if (m_nCurrentBook == m_vCurrentNumbers.size())
		{
			m_pRightPage->setVisible(true);
			m_pInfoButton->setVisible(false);
			m_pInfoButtonClose->setVisible(false);
			this->runAction(Sequence::create(
				DelayTime::create(0.5f)
				, CallFunc::create(CC_CALLBACK_0(castle::CScene::onShowInfoBook, this, true))
				, nullptr));
		}
	};

	if (type == ui::Widget::TouchEventType::BEGAN && !m_bInfoBookIsOpened)
	{
		for (const auto& book : m_vBooks)
		{
			if (book.first->_ID == sender->_ID)
			{
				if (book.second == m_vCurrentNumbers[m_nCurrentBook])
				{
					m_nCurrentBook++;

					book.first->setTouchEnabled(false);
					book.first->runAction(Sequence::create(
						FadeOut::create(0.5f)
						, CallFunc::create(onEnd)
						, nullptr));
				}
				else
				{
					m_nFails++;

					if (m_nFails == 5u)
					{
						setTouchesAllBooks(false);
						setNextScene(m_szSceneName);
						onShownGrayLayer(Color3B::WHITE, 0.45f);
					}
				}
			}

			if (m_nCurrentBook == m_vCurrentNumbers.size() )
			{
				setTouchesAllBooks(false);
			}
		}
	}
}

void castle::LibraryScene::setTouchesAllBooks(bool bEnable)
{
	for (const auto& book : m_vBooks)
	{
		book.first->setTouchEnabled(false);
	}
}
