#include "pch.h"

namespace castle
{
	class LibraryScene : public CScene
	{
	public:
		LibraryScene() = default;
		virtual ~LibraryScene() = default;

	public:
		static CScene* createScene();

		virtual bool init();

		virtual void onClosedBook();

		virtual void onEnter();

		CREATE_FUNC(LibraryScene);

	private:
		bool addBook(const std::string & szPath, Vec2 vPosition);
		void clickBook(Ref* sender, Widget::TouchEventType type);

		void setTouchesAllBooks(bool bEnable);

	protected:
		uint32_t m_nFails = 0u;
		uint32_t m_nNeedOpen = 0u;
		uint32_t m_nCurrentBook = 0u;
		std::vector<uint32_t> m_vBookNumbers;
		std::vector<uint32_t> m_vCurrentNumbers;

		Label* m_pInfoTextWidget = nullptr;
		Label* m_pFirstTextWidget = nullptr;
		Button* m_pButtonExit = nullptr;
		Sprite* m_pBookcase = nullptr;
		std::map<Button*, uint32_t> m_vBooks;
	};
}