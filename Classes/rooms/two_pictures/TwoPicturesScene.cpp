#include "rooms/two_pictures/TwoPicturesScene.h"

using namespace castle;

CScene* TwoPicturesScene::createScene()
{
	return TwoPicturesScene::create();
}

bool TwoPicturesScene::init()
{
	bool bResult = CScene::init();

	m_szSceneName = sz_twoPicturesScene;

	m_pArks = Sprite::create("rooms/two_pictures/pictures_arks.png");
	m_pPictures = Sprite::create("rooms/two_pictures/pictures_two_pictures.png");
	m_pHoles = Sprite::create("rooms/two_pictures/pictures_holes.png");
	auto textWidget = Label::createWithTTF("I ended up in an unusual room with two identical paintings. Although stop, it seems to me or are they different?", "fonts/marker-felt.ttf", 85);
	auto textWidget2 = Label::createWithTTF("Congratulations!", "fonts/marker-felt.ttf", 85);
	auto textOK = Label::createWithTTF("OK!", "fonts/marker-felt.ttf", 65);
	m_pButtonExit = Button::create("button_idle.png", "button_push.png");
	auto layoutTextAndButton = Layout::create();

	bResult &= setBackground("rooms/two_pictures/pictures_background.png");
	bResult &= m_pArks != nullptr;
	bResult &= m_pPictures != nullptr;
	bResult &= m_pHoles != nullptr;
	bResult &= textWidget != nullptr;
	bResult &= textWidget2 != nullptr;
	bResult &= m_pButtonExit != nullptr;
	bResult &= textOK != nullptr;
	bResult &= layoutTextAndButton != nullptr;

	if (bResult)
	{
		bResult &= addCircle(Vec2(0.19f, 0.2f), Vec2(0.65f, 0.2f));
		bResult &= addCircle(Vec2(0.24f, 0.3f), Vec2(0.7f, 0.3f));
		bResult &= addCircle(Vec2(0.26f, 0.2525f), Vec2(0.72f, 0.2525f));
		bResult &= addCircle(Vec2(0.285f, 0.365f), Vec2(0.745f, 0.365f));
		bResult &= addCircle(Vec2(0.3075f, 0.6f), Vec2(0.7675f, 0.6f));
		bResult &= addCircle(Vec2(0.3575f, 0.3f), Vec2(0.8175f, 0.3f));
	}

	if (bResult)
	{
		this->addChild(m_pHoles, 0);
		this->addChild(m_pPictures, 1);
		this->addChild(m_pArks, 2);

		m_pArks->setPosition(Vec2(m_VisibleSize.width / 2 + m_VisibleOrigin.x, m_VisibleSize.height / 2 + m_VisibleOrigin.y));
		m_pArks->setScale(m_VisibleSize.height / m_pArks->getContentSize().height * 1.1f);

		setCopySizeScalePosition(m_pPictures, m_pArks);
		setCopySizeScalePosition(m_pHoles, m_pArks);

		layoutTextAndButton->setContentSize(Size(700, 900));
		layoutTextAndButton->setAnchorPoint(Vec2(0.5, 0.5));

		layoutTextAndButton->addChild(textWidget2);
		layoutTextAndButton->addChild(m_pButtonExit);

		textWidget->setColor(Color3B::BLACK);
		textWidget2->setColor(Color3B::BLACK);
		textOK->setColor(Color3B(200, 200, 200));

		textWidget->setDimensions(700, 900);
		//textWidget2->setDimensions(700, 450);
		textWidget2->setPosition(getPositionAboutParrent(textWidget2, Vec2(0.5, 0.75f)));

		m_pButtonExit->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_pButtonExit->setColor(Color3B(105, 160, 255));
		m_pButtonExit->setPosition(getPositionAboutParrent(m_pButtonExit, Vec2(0.5, 0.25f)));

		m_pButtonExit->setTitleLabel(textOK);
		m_pButtonExit->setScale(1.5f);

		m_pButtonExit->addTouchEventListener([this](Ref* sender, Widget::TouchEventType type)
		{
			if (type == ui::Widget::TouchEventType::ENDED)
			{
				m_pButtonExit->setTouchEnabled(false);
				onShownGrayLayer(Color3B::BLACK, 0.5f);
			}
		});

		layoutTextAndButton->setVisible(false);

		m_pLeftPage = textWidget;
		m_pRightPage = layoutTextAndButton;


		createInfoBook("book_1.png");
	}

	return bResult;
}

void TwoPicturesScene::onEnter()
{
	CScene::onEnter();
	if (m_pInfoButton != nullptr)
		m_pInfoButton->setVisible(true);
}

bool TwoPicturesScene::addCircle(Vec2 vPosition, Vec2 vPosition2)
{
	auto pCircle = Button::create("rooms/two_pictures/circle_empty.png");
	auto pCircle2 = Button::create("rooms/two_pictures/circle_empty.png");
	bool bResult = pCircle != nullptr;
	bResult = pCircle2 != nullptr;

	if (bResult)
	{
		Color3B color;
		color = Color3B(Color4F(cocos2d::random(0.f, 1.f) * 1.f + 0.0f,
			cocos2d::random(0.f, 1.f) * 1.f + 0.0f,
			cocos2d::random(0.f, 1.f) * 1.f + 0.0f,
			1));

		m_pPictures->addChild(pCircle);
		m_pPictures->addChild(pCircle2);

		pCircle->setColor(color);
		pCircle2->setColor(color);
		
		pCircle->setAnchorPoint(Vec2(0.5f, 0.5f));
		pCircle2->setAnchorPoint(Vec2(0.5f, 0.5f));

		pCircle->setPosition(getPositionAboutParrent(pCircle, vPosition));
		pCircle2->setPosition(getPositionAboutParrent(pCircle2, vPosition2));

		pCircle->addTouchEventListener(CC_CALLBACK_2(castle::TwoPicturesScene::clickCircle, this));
		pCircle2->addTouchEventListener(CC_CALLBACK_2(castle::TwoPicturesScene::clickCircle, this));

		m_cCorrects.push_back(std::pair<Button* , Button*>(pCircle, pCircle2));
	}

	return bResult;
}

void castle::TwoPicturesScene::clickCircle(Ref * sender, Widget::TouchEventType type)
{
	if (type == ui::Widget::TouchEventType::BEGAN && !m_bProcessed && !m_bInfoBookIsOpened)
	{
		m_bProcessed = true;

		for (const auto& circles : m_cCorrects)
		{
			if (circles.first->_ID == sender->_ID
				|| circles.second->_ID == sender->_ID)
			{
				circles.first->setTouchEnabled(false);
				circles.second->setTouchEnabled(false);

				circles.first->loadTextureNormal("rooms/two_pictures/circle.png");
				circles.second->loadTextureNormal("rooms/two_pictures/circle.png");

				m_nCorrectsCount++;

				this->runAction(Sequence::create(
					DelayTime::create(0.01f)
					, CallFunc::create([this]() {m_bProcessed = false; })
					, nullptr));
			}
		}

		if (m_nCorrectsCount == m_cCorrects.size())
		{
			m_pRightPage->setVisible(true);

			m_pInfoButton->setVisible(false);
			m_pInfoButtonClose->setVisible(false);
			onShowInfoBook(true);
		}
	}

}
