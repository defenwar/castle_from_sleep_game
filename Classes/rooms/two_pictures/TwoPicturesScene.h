#include "pch.h"

namespace castle
{
	class TwoPicturesScene : public CScene
	{
	public:
		TwoPicturesScene() = default;
		virtual ~TwoPicturesScene() = default;

	public:
		static CScene* createScene();

		virtual bool init();

		virtual void onEnter();

		CREATE_FUNC(TwoPicturesScene);

	private:
		bool addCircle(Vec2 vPosition, Vec2 vPosition2);
		
		void clickCircle(Ref* sender, Widget::TouchEventType type);

	protected:
		bool m_bProcessed = false;

		Sprite* m_pArks = nullptr;
		Sprite* m_pPictures = nullptr;
		Sprite* m_pHoles = nullptr;

		Button* m_pButtonExit = nullptr;

		uint32_t m_nCorrectsCount = 0u;
		std::vector<std::pair<Button*, Button*>> m_cCorrects;
	};
}