#include "rooms/pat/PatScene.h"

using namespace castle;

CScene* PatScene::createScene()
{
	return PatScene::create();
}

bool PatScene::init()
{
	bool bResult = CScene::init();

	m_szSceneName = sz_patScene;

	m_pDop = Sprite::create("rooms/pat/pat_dop.png");
	m_pPolki = Sprite::create("rooms/pat/pat_polki.png");

	auto textWidget = Label::createWithTTF("I ended up in a room with different bottles, if I try to open them, then they behave funny, it's worth trying to open them all.", "fonts/marker-felt.ttf", 85);
	auto textWidget2 = Label::createWithTTF("Congratulations!", "fonts/marker-felt.ttf", 85);
	auto textOK = Label::createWithTTF("OK!", "fonts/marker-felt.ttf", 65);
	m_pButtonExit = Button::create("button_idle.png", "button_push.png");
	auto layoutTextAndButton = Layout::create();

	bResult &= setBackground("rooms/pat/pat_background.png");
	bResult &= m_pDop != nullptr;
	bResult &= m_pPolki != nullptr;
	bResult &= textWidget != nullptr;
	bResult &= textWidget2 != nullptr;
	bResult &= m_pButtonExit != nullptr;
	bResult &= textOK != nullptr;
	bResult &= layoutTextAndButton != nullptr;

	if (bResult)
	{
		bResult &= addIngredient("rooms/pat/ingredients/1.png",  Vec2(0.2f,  0.14f));
		bResult &= addIngredient("rooms/pat/ingredients/2.png",  Vec2(0.53f, 0.14f));
		bResult &= addIngredient("rooms/pat/ingredients/3.png",  Vec2(0.6f,  0.14f));
		bResult &= addIngredient("rooms/pat/ingredients/4.png",  Vec2(0.7f,  0.14f));
		bResult &= addIngredient("rooms/pat/ingredients/5.png",  Vec2(0.9f,  0.14f));

		bResult &= addIngredient("rooms/pat/ingredients/6.png",  Vec2(0.1f, 0.36f));
		bResult &= addIngredient("rooms/pat/ingredients/7.png",  Vec2(0.3f, 0.36f));
		bResult &= addIngredient("rooms/pat/ingredients/8.png",  Vec2(0.5f, 0.36f));
		bResult &= addIngredient("rooms/pat/ingredients/9.png",  Vec2(0.7f, 0.36f));
		bResult &= addIngredient("rooms/pat/ingredients/10.png", Vec2(0.9f, 0.36f));

		bResult &= addIngredient("rooms/pat/ingredients/11.png", Vec2(0.1f,  0.583f));
		bResult &= addIngredient("rooms/pat/ingredients/12.png", Vec2(0.25f, 0.583f));
		bResult &= addIngredient("rooms/pat/ingredients/13.png", Vec2(0.4f,  0.583f));
		bResult &= addIngredient("rooms/pat/ingredients/14.png", Vec2(0.55f, 0.583f));
		bResult &= addIngredient("rooms/pat/ingredients/15.png", Vec2(0.7f,  0.583f));
		bResult &= addIngredient("rooms/pat/ingredients/16.png", Vec2(0.85f, 0.583f));

		bResult &= addIngredient("rooms/pat/ingredients/17.png", Vec2(0.27f, 0.81f));
		bResult &= addIngredient("rooms/pat/ingredients/18.png", Vec2(0.37f, 0.81f));
		bResult &= addIngredient("rooms/pat/ingredients/19.png", Vec2(0.55f, 0.81f));
		bResult &= addIngredient("rooms/pat/ingredients/20.png", Vec2(0.65f, 0.81f));
		bResult &= addIngredient("rooms/pat/ingredients/21.png", Vec2(0.72f, 0.81f));
		bResult &= addIngredient("rooms/pat/ingredients/22.png", Vec2(0.90f, 0.81f));
	}


	if (bResult)
	{
		this->addChild(m_pPolki, 0);
		this->addChild(m_pDop, 4);

		m_pPolki->setScale(m_VisibleSize.height / m_pPolki->getContentSize().height);
		m_pPolki->setAnchorPoint(Vec2(0.f, 1.f));
		m_pPolki->setPosition(Vec2(0.f, m_VisibleSize.height));
		setCopySizeScalePosition(m_pDop, m_pBackground);

		layoutTextAndButton->setContentSize(Size(700, 900));
		layoutTextAndButton->setAnchorPoint(Vec2(0.5, 0.5));

		layoutTextAndButton->addChild(textWidget2);
		layoutTextAndButton->addChild(m_pButtonExit);

		textWidget->setColor(Color3B::BLACK);
		textWidget2->setColor(Color3B::BLACK);
		textOK->setColor(Color3B(200, 200, 200));

		textWidget->setDimensions(700, 900);
		textWidget2->setPosition(getPositionAboutParrent(textWidget2, Vec2(0.5, 0.75f)));

		m_pButtonExit->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_pButtonExit->setColor(Color3B(105, 160, 255));
		m_pButtonExit->setPosition(getPositionAboutParrent(m_pButtonExit, Vec2(0.5, 0.25f)));

		m_pButtonExit->setTitleLabel(textOK);
		m_pButtonExit->setScale(1.5f);

		m_pButtonExit->addTouchEventListener([this](Ref* sender, Widget::TouchEventType type)
		{
			if (type == ui::Widget::TouchEventType::ENDED)
			{
				m_pButtonExit->setTouchEnabled(false);
				onShownGrayLayer(Color3B::BLACK, 0.5f);
			}
		});

		layoutTextAndButton->setVisible(false);

		m_pLeftPage = textWidget;
		m_pRightPage = layoutTextAndButton;


		createInfoBook("book_1.png");
	}

	return bResult;
}

void PatScene::onEnter()
{
	CScene::onEnter();
	if (m_pInfoButton != nullptr)
		m_pInfoButton->setVisible(true);
}

cocos2d::FiniteTimeAction* PatScene::getActionIngredient(uint32_t indx)
{
	float fTime = 1.75f;

	switch (indx)
	{
	case 0u:
		return EaseCircleActionIn::create(RotateBy::create(fTime, 360.f));
		break;
	case 1u:
		return EaseCircleActionIn::create(RotateBy::create(fTime, -360.f));
		break;
	case 2u:
		return EaseCircleActionOut::create(RotateBy::create(fTime, 360.f));
		break;
	case 3u:
		return EaseCircleActionOut::create(RotateBy::create(fTime, -360.f));
		break;
	case 4u:
		return EaseCircleActionInOut::create(RotateBy::create(fTime, 360.f));
		break;
	case 5u:
		return EaseElasticIn::create(RotateBy::create(fTime, 360.f));
		break;
	case 6u:
		return EaseElasticIn::create(RotateBy::create(fTime, -360.f));
		break;
	case 7u:
		return EaseElasticOut::create(RotateBy::create(fTime, 360.f));
		break;
	case 8u:
		return EaseElasticOut::create(RotateBy::create(fTime, -360.f));
		break;
	case 9u:
		return EaseElasticInOut::create(RotateBy::create(fTime, 360.f));
		break;
	case 10u:

		return EaseBackIn::create(MoveBy::create(fTime, Vec2(200, 0)));
		break;
	case 11u:
		return EaseBackIn::create(MoveBy::create(fTime, Vec2(100, -100)));
		break;
	case 12u:
		return EaseBackOut::create(MoveBy::create(fTime, Vec2(200, 0)));
		break;
	case 13u:
		return EaseBackOut::create(MoveBy::create(fTime, Vec2(100, -100)));
		break;
	case 14u:
		return EaseBackInOut::create(MoveBy::create(fTime, Vec2(200, 0)));
		break;
	case 15u:
		return EaseBackIn::create(JumpBy::create(fTime, Vec2(0, 0), 100.f, 3));
		break;
	case 16u:
		return EaseBackOut::create(JumpBy::create(fTime, Vec2(100, 0), 100.f, 3));
		break;
	case 17u:
		return EaseBackOut::create(JumpBy::create(fTime, Vec2(200, 0), 100.f, 1));
		break;
	case 18u:
		return EaseBackInOut::create(JumpBy::create(fTime, Vec2(100, 0), 100.f, 3));
		break;
	case 19u:
		return EaseBackIn::create(JumpBy::create(fTime, Vec2(50, 0), 100.f, 3));
		break;
	case 20u:
		return Sequence::create(
			EaseCircleActionIn::create(RotateBy::create(fTime, 360.f))
			, ScaleBy::create(0.5f, 2.f)
			, ScaleBy::create(0.5f, 0.5f)
			, nullptr);
		break;
	case 21u:
		return Sequence::create(
			EaseCircleActionIn::create(RotateBy::create(fTime, 360.f))
			, SkewBy::create(0.5f, 20.f, 20.f)
			, SkewBy::create(0.5f, -20.f, -20.f)
			, nullptr);
		break;
	default:
		return RotateBy::create(fTime, 1440.f);
		break;
	}
}

bool PatScene::addIngredient(const std::string& szPath, Vec2 vPosition)
{
	auto pIngredient = Button::create(szPath);
	bool bResult = pIngredient != nullptr;

	if (bResult)
	{
		m_pPolki->addChild(pIngredient);
		pIngredient->setAnchorPoint(Vec2(0.5f, 0.f));
		pIngredient->setPosition(getPositionAboutParrent(pIngredient, vPosition));
		pIngredient->addTouchEventListener(CC_CALLBACK_2(castle::PatScene::clickIngredient, this));
		m_vIngredients.push_back(pIngredient);
	}

	return bResult;
}

void castle::PatScene::clickIngredient(Ref * sender, Widget::TouchEventType type)
{
	auto onEnd = [this]() 
	{
		m_nIngredientsTouched++;

		if (m_nIngredientsTouched == m_vIngredients.size())
		{
			m_pRightPage->setVisible(true);
			m_pInfoButton->setVisible(false);
			m_pInfoButtonClose->setVisible(false);
			this->runAction(Sequence::create(
				DelayTime::create(0.5f)
				, CallFunc::create(CC_CALLBACK_0(castle::CScene::onShowInfoBook, this, true))
				, nullptr));
		}
	};

	if (type == ui::Widget::TouchEventType::ENDED && !m_bInfoBookIsOpened)
	{
		for (uint32_t indx = 0u; indx < m_vIngredients.size(); indx++)
		{
			if (m_vIngredients[indx]->_ID == sender->_ID)
			{
				auto tempAction =	getActionIngredient(indx);
				m_vIngredients[indx]->setTouchEnabled(false);
				m_vIngredients[indx]->runAction(Sequence::create(
					tempAction
					, DelayTime::create(0.2f)
					, FadeOut::create(0.5f)
					, CallFunc::create(onEnd)
					, nullptr));
			}
		}
	}
}
