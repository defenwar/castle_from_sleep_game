#include "pch.h"

namespace castle
{
	class PatScene : public CScene
	{
	public:
		PatScene() = default;
		virtual ~PatScene() = default;

	public:
		static CScene* createScene();

		virtual bool init();

		virtual void onEnter();



		CREATE_FUNC(PatScene);

	private:
		bool addIngredient(const std::string & szPath, Vec2 vPosition);
		cocos2d::FiniteTimeAction * getActionIngredient(uint32_t indx);

		void clickIngredient(Ref* sender, Widget::TouchEventType type);

	protected:
		uint32_t m_nIngredientsTouched = 0u;

		Sprite* m_pDop = nullptr;
		Sprite* m_pPolki = nullptr;

		Button* m_pButtonExit = nullptr;

		std::vector<Button*> m_vIngredients;
	};
}