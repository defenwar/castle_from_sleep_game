#include "pch.h"

namespace castle
{
	class MramorRoomScene : public CScene
	{
	public:
		MramorRoomScene() = default;
		virtual ~MramorRoomScene() = default;

	public:
		static CScene* createScene();

		virtual bool init();

		virtual void onEnter();

		CREATE_FUNC(MramorRoomScene);

	private:
		bool fillInfoBook();
		bool addButtons();
		bool addFigures();

		void clickButton(Ref* sender, Widget::TouchEventType type);

		void moveFigure(uint32_t nIndexFigure);
		cocos2d::FiniteTimeAction * getActionForFigure(uint32_t nIndexFigure, bool bShow);

		bool checkStateFigures();

		void onEndedGame();

	protected:
		bool m_bProcessed = false;
		uint32_t m_nCountMovingFigures = 0u;
		Sprite* m_pGlassWindow = nullptr;

		Sprite* m_pPillar1 = nullptr;
		Sprite* m_pPillar2 = nullptr;
		Sprite* m_pPillar3 = nullptr;

		Button* m_pButtonExit = nullptr;

		std::vector<bool> m_vStateFigures;
		std::vector<Sprite*> m_vFigures;
		std::vector<Button*> m_vButtons;
	};
}
