#include "rooms/mramor/MramorRoomScene.h"

using namespace castle;

CScene* MramorRoomScene::createScene()
{
	return MramorRoomScene::create();
}

bool MramorRoomScene::init()
{
	bool bResult = CScene::init();

	m_szSceneName = sz_mramorScene;

	m_pGlassWindow = Sprite::create("rooms/mramor/mramor_glass_window.png");

	m_pPillar3 = Sprite::create("rooms/mramor/mramor_pillar_3.png");
	m_pPillar2 = Sprite::create("rooms/mramor/mramor_pillar_2.png");
	m_pPillar1 = Sprite::create("rooms/mramor/mramor_pillar_1.png");

	bResult &= setBackground("rooms/mramor/mramor_bg.png");
	bResult &= m_pGlassWindow != nullptr;

	bResult &= m_pPillar3 != nullptr;
	bResult &= m_pPillar2 != nullptr;
	bResult &= m_pPillar1 != nullptr;


	if (bResult)
	{
		this->addChild(m_pPillar3, 0);
		this->addChild(m_pPillar2, 2);
		this->addChild(m_pPillar1, 4);

		m_pBackground->addChild(m_pGlassWindow, 0);
		m_pGlassWindow->setPosition(getPositionAboutParrent(m_pGlassWindow, Vec2(0.498, 0.475)));

		setCopySizeScalePosition(m_pPillar3, m_pBackground);
		setCopySizeScalePosition(m_pPillar2, m_pBackground);
		setCopySizeScalePosition(m_pPillar1, m_pBackground);

		bResult &= addButtons();
		bResult &= addFigures();
		bResult &= fillInfoBook();
	}

	return bResult;
}

void MramorRoomScene::onEnter()
{
	CScene::onEnter();
	if (m_pInfoButton != nullptr)
		m_pInfoButton->setVisible(true);

}

bool castle::MramorRoomScene::fillInfoBook()
{
	bool bResult = true;

	auto textWidget = Label::createWithTTF("A strange room with statues and buttons.If you press a button, one or more statues move in or out.We need to find a way to push all the statues.", "fonts/marker-felt.ttf", 85);
	auto textWidget2 = Label::createWithTTF("Congratulations!", "fonts/marker-felt.ttf", 85);
	auto textOK = Label::createWithTTF("OK!", "fonts/marker-felt.ttf", 65);
	m_pButtonExit = Button::create("button_idle.png", "button_push.png");
	auto layoutTextAndButton = Layout::create();

	bResult &= textWidget != nullptr;
	bResult &= textWidget2 != nullptr;
	bResult &= m_pButtonExit != nullptr;
	bResult &= textOK != nullptr;
	bResult &= layoutTextAndButton != nullptr;

	if (bResult)
	{
		textWidget->setDimensions(700, 900);
		textWidget->setColor(Color3B::BLACK);

		layoutTextAndButton->setContentSize(Size(700, 900));
		layoutTextAndButton->setAnchorPoint(Vec2(0.5, 0.5));

		layoutTextAndButton->addChild(textWidget2);
		layoutTextAndButton->addChild(m_pButtonExit);

		textWidget->setColor(Color3B::BLACK);
		textWidget2->setColor(Color3B::BLACK);
		textOK->setColor(Color3B(200, 200, 200));

		//textWidget2->setDimensions(700, 450);
		textWidget2->setPosition(getPositionAboutParrent(textWidget2, Vec2(0.5, 0.75f)));

		m_pButtonExit->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_pButtonExit->setColor(Color3B(105, 160, 255));
		m_pButtonExit->setPosition(getPositionAboutParrent(m_pButtonExit, Vec2(0.5, 0.25f)));

		m_pButtonExit->setTitleLabel(textOK);
		m_pButtonExit->setScale(1.5f);

		m_pButtonExit->addTouchEventListener([this](Ref* sender, Widget::TouchEventType type)
		{
			if (type == ui::Widget::TouchEventType::ENDED)
			{
				m_pButtonExit->setTouchEnabled(false);
				onShownGrayLayer(Color3B::BLACK, 0.5f);
			}
		});

		layoutTextAndButton->setVisible(false);

		m_pLeftPage = textWidget;
		m_pRightPage = layoutTextAndButton;

		createInfoBook("book_1.png");
	}

	return bResult;
}

bool castle::MramorRoomScene::addButtons()
{
	bool bResult = true;

	auto pButton1 = Button::create("button_idle.png", "button_push.png");
	auto pButton2 = Button::create("button_idle.png", "button_push.png");
	auto pButton3 = Button::create("button_idle.png", "button_push.png");
	auto pButton4 = Button::create("button_idle.png", "button_push.png");

	if (bResult)
	{
		bResult &= pButton1 != nullptr;
		bResult &= pButton2 != nullptr;
		bResult &= pButton3 != nullptr;
		bResult &= pButton4 != nullptr;

		m_pBackground->addChild(pButton1, 1);
		m_pBackground->addChild(pButton2, 1);
		m_pBackground->addChild(pButton3, 1);
		m_pBackground->addChild(pButton4, 1);
		
		pButton1->setColor(Color3B::GREEN);
		pButton2->setColor(Color3B::YELLOW);
		pButton3->setColor(Color3B::RED);
		pButton4->setColor(Color3B::BLUE);

		pButton1->setScale(0.65f);
		pButton2->setScale(0.65f);
		pButton3->setScale(0.65f);
		pButton4->setScale(0.65f);

		pButton1->setAnchorPoint(Vec2(0.5f, 0.5f));
		pButton2->setAnchorPoint(Vec2(0.5f, 0.5f));
		pButton3->setAnchorPoint(Vec2(0.5f, 0.5f));
		pButton4->setAnchorPoint(Vec2(0.5f, 0.5f));

		pButton1->setPosition(getPositionAboutParrent(pButton1, Vec2(0.39, 0.36)));
		pButton2->setPosition(getPositionAboutParrent(pButton1, Vec2(0.39, 0.55)));
		pButton3->setPosition(getPositionAboutParrent(pButton1, Vec2(0.61, 0.55)));
		pButton4->setPosition(getPositionAboutParrent(pButton1, Vec2(0.61, 0.36)));

		pButton1->addTouchEventListener(CC_CALLBACK_2(castle::MramorRoomScene::clickButton, this));
		pButton2->addTouchEventListener(CC_CALLBACK_2(castle::MramorRoomScene::clickButton, this));
		pButton3->addTouchEventListener(CC_CALLBACK_2(castle::MramorRoomScene::clickButton, this));
		pButton4->addTouchEventListener(CC_CALLBACK_2(castle::MramorRoomScene::clickButton, this));

		m_vButtons.push_back(pButton1);
		m_vButtons.push_back(pButton2);
		m_vButtons.push_back(pButton3);
		m_vButtons.push_back(pButton4);
	}

	return bResult;
}

bool castle::MramorRoomScene::addFigures()
{
	bool bResult = true;

	auto pFigure1 = Sprite::create("rooms/mramor/mramor_figure_1.png");
	auto pFigure2 = Sprite::create("rooms/mramor/mramor_figure_3.png");
	auto pFigure3 = Sprite::create("rooms/mramor/mramor_figure_4.png");
	auto pFigure4 = Sprite::create("rooms/mramor/mramor_figure_2.png");

	bResult &= pFigure1 != nullptr;
	bResult &= pFigure2 != nullptr;
	bResult &= pFigure3 != nullptr;
	bResult &= pFigure4 != nullptr;

	if (bResult)
	{
		m_pPillar2->addChild(pFigure1, 3);
		m_pPillar3->addChild(pFigure2, 1);
		m_pPillar3->addChild(pFigure3, 1);
		m_pPillar2->addChild(pFigure4, 3);

		pFigure1->setPosition(getPositionAboutParrent(pFigure1, Vec2(0.16, 0.4)));
		pFigure2->setPosition(getPositionAboutParrent(pFigure2, Vec2(0.26, 0.37)));
		pFigure3->setPosition(getPositionAboutParrent(pFigure3, Vec2(0.74, 0.3)));
		pFigure4->setPosition(getPositionAboutParrent(pFigure4, Vec2(0.84, 0.33)));

		m_vFigures.push_back(pFigure1);
		m_vFigures.push_back(pFigure2);
		m_vFigures.push_back(pFigure3);
		m_vFigures.push_back(pFigure4);

		m_vStateFigures.resize(m_vFigures.size(), false);
	}

	return bResult;
}

void castle::MramorRoomScene::clickButton(Ref * sender, Widget::TouchEventType type)
{
	if (type == ui::Widget::TouchEventType::ENDED && !m_bProcessed && !m_bInfoBookIsOpened)
	{
		m_bProcessed = true;

		for (const auto& button : m_vButtons)
		{
			button->setTouchEnabled(false);
		}

		if (sender->_ID == m_vButtons[0]->_ID)
		{
			moveFigure(0u);
			moveFigure(2u);
		}

		if (sender->_ID == m_vButtons[1]->_ID)
		{
			moveFigure(0u);
			moveFigure(1u);
			moveFigure(2u);
		}

		if (sender->_ID == m_vButtons[2]->_ID)
		{
			moveFigure(1u);
			moveFigure(2u);
		}

		if (sender->_ID == m_vButtons[3]->_ID)
		{
			moveFigure(2u);
			moveFigure(3u);
		}
	}
}

void castle::MramorRoomScene::moveFigure(uint32_t nIndexFigure)
{
	auto onEnd = [this]() 
	{
		if (m_nCountMovingFigures != 0u)
		{
			m_nCountMovingFigures--;
			if (m_nCountMovingFigures == 0u)
				if (checkStateFigures())
				{
					onEndedGame();
				}
				else
				{
					m_bProcessed = false;

					for (const auto& button : m_vButtons)
					{
						button->setTouchEnabled(true);
					}
				}
		}
	};

	if (nIndexFigure < m_vFigures.size())
	{
		m_nCountMovingFigures++;
		auto actionFigure = getActionForFigure(nIndexFigure, m_vStateFigures[nIndexFigure]);

		m_vFigures[nIndexFigure]->runAction(Sequence::create(
			actionFigure
			, CallFunc::create(onEnd)
			, nullptr));

		m_vStateFigures[nIndexFigure] = !m_vStateFigures[nIndexFigure];
	}
}

cocos2d::FiniteTimeAction* castle::MramorRoomScene::getActionForFigure(uint32_t nIndexFigure, bool bShow)
{
	cocos2d::FiniteTimeAction* action = nullptr;

	switch (nIndexFigure)
	{
	case 0u:
	case 1u:
		if (bShow)
		{
			action = Spawn::create(
				MoveBy::create(0.5f, Vec2(200.f, 0.f))
				, FadeIn::create(0.5f)
				, nullptr);
		}
		else
		{
			action = Spawn::create(
				MoveBy::create(0.5f, Vec2(-200.f, 0.f))
				, FadeOut::create(0.5f)
				, nullptr);
		}
		break;

	case 2u:
	case 3u:
		if (bShow)
		{
			action = Spawn::create(
				MoveBy::create(0.5f, Vec2(-200.f, 0.f))
				, FadeIn::create(0.5f)
				, nullptr);
		}
		else
		{
			action = Spawn::create(
				MoveBy::create(0.5f, Vec2(200.f, 0.f))
				, FadeOut::create(0.5f)
				, nullptr);
		}
		break;

	default:
		break;
	}

	return action;
}

bool castle::MramorRoomScene::checkStateFigures()
{
	for (const auto& figureState: m_vStateFigures)
	{
		if (!figureState)
			return false;
	}

	return true;
}

void castle::MramorRoomScene::onEndedGame()
{
	auto onEnd = [this]()
	{
		m_pRightPage->setVisible(true);
		m_pInfoButton->setVisible(false);
		m_pInfoButtonClose->setVisible(false);
		onShowInfoBook(true);
	};

	m_pGlassWindow->runAction(Sequence::create(
		FadeOut::create(0.5f)
		, DelayTime::create(0.5f)
		, CallFunc::create(onEnd)
		, nullptr));
}